# demo-auto-generator
> 此项目为前后端分离的 Spring Boot 项目
> 前端：nodejs + VUE
> 后端：Spring Boot + MySQL + Redis
> 运行环境：jdk8 + Maven 3.6.3

## I. 生成的内容
1. 生成 SQL 文件
2. 生成 Model 层代码
3. 生成 Mapper 层的代码：Mapper Interface & Mapper Xml
4. 生成 Service 层代码
5. 生成 Controller 层代码（不带自定义回复结果）

## II. Model录入要求
a. 命名格式：
```
1. [组织名.]项目名称.模块名.实体名
    1. 组织名可以没有，但建议加上，若没有，则使用默认的，即：com.example
    2. 项目名称、模块名和实体名称为必填项
    3. 所有的名称命名必须使用字符、数字或下划线，且必须以字母开头
    4. 实体名还必须符合驼峰命名格式，应该尽量做到见名知义；
       实体名应该以T、V或S开头，其中T表示数据表，V表示视图，S表示序列
	
    合理的案例：
    	com.example.demo.basic.TBasicModel 表示demo项目中的基础数据模块的模型表

2. 模型明细录入
    1. 属性项为必填项，而列名项为可选录入
    2. 属性名必须满足【java 命名格式】
```

## III. 部署运行
### 1. 基本的软件安装

* nodejs（尽量安装最新版本v12.0及以后）
* mysql（5.7 及以后）
* redis
* maven（3.6.x 及以后）



### 2. 后端项目

```
打开Idea导入项目即可，数据库配置在application.yml中，运行前请修改成自己的配置
1. 运行数据库脚本文件：
	demo-generator-backend/doc/db-sc.sql
2. 修改数据库连接的参数（mysql、redis）： 
	application.yml
```



### 3. 前端项目

```
1. 安装好 nodejs，切换到项目的根目录下
2. 配置国内的镜像源：
npm config set registry https://registry.npm.taobao.org
npm config get registry
npm install -g cnpm --registry=https://registry.npm.taobao.org

（可选）3. 前端跨域访问在 demo-generator-front/vue.config.js 中配置（一般只会要修改IP或端口）

4. 项目编译运行
npm install
npm run serve
```



