/*
 Navicat MySQL Data Transfer

 Source Server         : xxx
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : localhost:3306
 Source Schema         : demo_generator_db

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 30/12/2020 17:51:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_demo_column_type
-- ----------------------------
DROP TABLE IF EXISTS `t_demo_column_type`;
CREATE TABLE `t_demo_column_type`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型名称',
  `len` int(11) NULL DEFAULT NULL COMMENT '字段类型长度',
  `default_len` int(11) NULL DEFAULT NULL COMMENT '字段类型默认长度',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `modify_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `modify_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `delete_flag` smallint(4) NULL DEFAULT NULL COMMENT '是否删除（1：是，0：否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字段类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_demo_column_type
-- ----------------------------
INSERT INTO `t_demo_column_type` VALUES ('791636597375643654', 'varchar', NULL, 32, 2, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643655', 'char', NULL, 240, 4, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643656', 'text', NULL, NULL, 6, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643657', 'int', NULL, NULL, 8, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643658', 'tinyint', NULL, NULL, 10, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643659', 'smallint', NULL, NULL, 12, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643660', 'bigint', NULL, NULL, 14, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643661', 'float', NULL, NULL, 16, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643662', 'double', NULL, NULL, 18, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643663', 'date', NULL, NULL, 20, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643664', 'datetime', NULL, NULL, 22, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643665', 'timestamp', NULL, NULL, 24, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` VALUES ('791636597375643666', 'json', NULL, NULL, 26, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);

-- ----------------------------
-- Table structure for t_demo_model
-- ----------------------------
DROP TABLE IF EXISTS `t_demo_model`;
CREATE TABLE `t_demo_model`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模型名称（带限定名）',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模型说明',
  `table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模型对应的表名称',
  `sort_no` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `model_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模型名称（不带限定名）',
  `package_dir` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级包路径',
  `package_dir2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '二级包路径',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `modify_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `modify_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `delete_flag` smallint(4) NULL DEFAULT NULL COMMENT '是否删除(1是,0否)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '模型（类）表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_demo_model
-- ----------------------------
INSERT INTO `t_demo_model` VALUES ('791674648206389248', 'edu.dublbo.demo.basic.TModel', '模型信息', 't_model', 1, 'TModel', 'edu.dublbo.demo.basic', 'edu.dublbo.demo', 'admin', '2020-12-24 06:32:25', 'admin', '2020-12-24 06:33:33', 0);
INSERT INTO `t_demo_model` VALUES ('791674869304930304', 'edu.dublbo.demo.basic.TModelDetail', '模型明细', 't_model_detail', 2, 'TModelDetail', 'edu.dublbo.demo.basic', 'edu.dublbo.demo', 'admin', '2020-12-24 06:33:17', 'admin', '2020-12-24 06:33:17', 0);
INSERT INTO `t_demo_model` VALUES ('793518221281341440', 'com.sdwz.wisepark.building.TBuildingPlan', '园区规划', 't_building_plan', 3, 'TBuildingPlan', 'com.sdwz.wisepark.building', 'com.sdwz.wisepark', 'admin', '2020-12-29 08:38:07', 'admin', '2020-12-29 08:38:07', 0);
INSERT INTO `t_demo_model` VALUES ('793518520830144512', 'com.sdwz.wisepark.building.TBuildingInfo', '建筑信息', 't_building_info', 4, 'TBuildingInfo', 'com.sdwz.wisepark.building', 'com.sdwz.wisepark', 'admin', '2020-12-29 08:39:18', 'admin', '2020-12-29 08:39:18', 0);

-- ----------------------------
-- Table structure for t_demo_model_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_demo_model_detail`;
CREATE TABLE `t_demo_model_detail`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `model_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属模型',
  `property_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性名（符合驼峰命名）',
  `property_type_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性类型ID',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性说明',
  `column_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表字段名',
  `column_type_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表字段类型ID',
  `column_length` int(11) NULL DEFAULT NULL COMMENT '表字段长度',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '序号',
  `inherent_flag` tinyint(4) NULL DEFAULT 0 COMMENT '是否时固有的标记（1：不能删除，0：可删除（默认））',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `modify_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `modify_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `delete_flag` smallint(6) NULL DEFAULT NULL COMMENT '是否删除（1是,0否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '模型明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_demo_model_detail
-- ----------------------------
INSERT INTO `t_demo_model_detail` VALUES ('791674648227360768', '791674648206389248', 'id', '791636597375643648', '主键ID', 'id', '791636597375643654', 32, 1, 1, 'admin', '2020-12-24 06:32:25', 'admin', '2020-12-24 06:32:25', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674648227360769', '791674648206389248', 'createUser', '791636597375643648', '创建人', 'create_user', '791636597375643654', 64, 996, 1, 'admin', '2020-12-24 06:32:25', 'admin', '2020-12-24 06:32:25', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674648227360770', '791674648206389248', 'createTime', '791636597375643650', '创建时间', 'create_time', '791636597375643664', NULL, 997, 1, 'admin', '2020-12-24 06:32:25', 'admin', '2020-12-24 06:32:25', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674648227360771', '791674648206389248', 'modifyUser', '791636597375643648', '最后更新人', 'modify_user', '791636597375643654', 64, 998, 1, 'admin', '2020-12-24 06:32:25', 'admin', '2020-12-24 06:32:25', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674648227360772', '791674648206389248', 'modifyTime', '791636597375643650', '最后更新时间', 'modify_time', '791636597375643664', NULL, 999, 1, 'admin', '2020-12-24 06:32:25', 'admin', '2020-12-24 06:32:25', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674648227360773', '791674648206389248', 'deleteFlag', '791636597375643649', '是否删除（1：是，0：否）', 'delete_flag', '791636597375643659', 4, 1000, 1, 'admin', '2020-12-24 06:32:25', 'admin', '2020-12-24 06:32:25', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674869313318912', '791674869304930304', 'id', '791636597375643648', '主键ID', 'id', '791636597375643654', 32, 1, 1, 'admin', '2020-12-24 06:33:17', 'admin', '2020-12-24 06:33:17', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674869313318913', '791674869304930304', 'createUser', '791636597375643648', '创建人', 'create_user', '791636597375643654', 64, 996, 1, 'admin', '2020-12-24 06:33:17', 'admin', '2020-12-24 06:33:17', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674869313318914', '791674869304930304', 'createTime', '791636597375643650', '创建时间', 'create_time', '791636597375643664', NULL, 997, 1, 'admin', '2020-12-24 06:33:17', 'admin', '2020-12-24 06:33:17', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674869313318915', '791674869304930304', 'modifyUser', '791636597375643648', '最后更新人', 'modify_user', '791636597375643654', 64, 998, 1, 'admin', '2020-12-24 06:33:17', 'admin', '2020-12-24 06:33:17', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674869313318916', '791674869304930304', 'modifyTime', '791636597375643650', '最后更新时间', 'modify_time', '791636597375643664', NULL, 999, 1, 'admin', '2020-12-24 06:33:17', 'admin', '2020-12-24 06:33:17', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791674869313318917', '791674869304930304', 'deleteFlag', '791636597375643649', '是否删除（1：是，0：否）', 'delete_flag', '791636597375643659', 4, 1000, 1, 'admin', '2020-12-24 06:33:17', 'admin', '2020-12-24 06:33:17', 0);
INSERT INTO `t_demo_model_detail` VALUES ('791676009681661952', '791674648206389248', 'name', '791636597375643648', '模型名称（带包路径）', 'name', '791636597375643654', 64, 2, 0, 'admin', '2020-12-24 06:37:49', 'admin', '2020-12-24 06:37:49', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518221302312960', '793518221281341440', 'id', '791636597375643648', '主键ID', 'id', '791636597375643654', 32, 1, 1, 'admin', '2020-12-29 08:38:07', 'admin', '2020-12-29 08:38:07', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518221302312961', '793518221281341440', 'createUser', '791636597375643648', '创建人', 'create_user', '791636597375643654', 64, 996, 1, 'admin', '2020-12-29 08:38:07', 'admin', '2020-12-29 08:38:07', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518221302312962', '793518221281341440', 'createTime', '791636597375643650', '创建时间', 'create_time', '791636597375643664', NULL, 997, 1, 'admin', '2020-12-29 08:38:07', 'admin', '2020-12-29 08:38:07', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518221302312963', '793518221281341440', 'modifyUser', '791636597375643648', '最后更新人', 'modify_user', '791636597375643654', 64, 998, 1, 'admin', '2020-12-29 08:38:07', 'admin', '2020-12-29 08:38:07', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518221302312964', '793518221281341440', 'modifyTime', '791636597375643650', '最后更新时间', 'modify_time', '791636597375643664', NULL, 999, 1, 'admin', '2020-12-29 08:38:07', 'admin', '2020-12-29 08:38:07', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518221302312965', '793518221281341440', 'deleteFlag', '791636597375643649', '是否删除（1：是，0：否）', 'delete_flag', '791636597375643659', 4, 1000, 1, 'admin', '2020-12-29 08:38:07', 'admin', '2020-12-29 08:38:07', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518520838533120', '793518520830144512', 'id', '791636597375643648', '主键ID', 'id', '791636597375643654', 32, 1, 1, 'admin', '2020-12-29 08:39:18', 'admin', '2020-12-29 08:39:18', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518520838533121', '793518520830144512', 'createUser', '791636597375643648', '创建人', 'create_user', '791636597375643654', 64, 996, 1, 'admin', '2020-12-29 08:39:18', 'admin', '2020-12-29 08:39:18', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518520838533122', '793518520830144512', 'createTime', '791636597375643650', '创建时间', 'create_time', '791636597375643664', NULL, 997, 1, 'admin', '2020-12-29 08:39:18', 'admin', '2020-12-29 08:39:18', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518520838533123', '793518520830144512', 'modifyUser', '791636597375643648', '最后更新人', 'modify_user', '791636597375643654', 64, 998, 1, 'admin', '2020-12-29 08:39:18', 'admin', '2020-12-29 08:39:18', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518520838533124', '793518520830144512', 'modifyTime', '791636597375643650', '最后更新时间', 'modify_time', '791636597375643664', NULL, 999, 1, 'admin', '2020-12-29 08:39:18', 'admin', '2020-12-29 08:39:18', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518520838533125', '793518520830144512', 'deleteFlag', '791636597375643649', '是否删除（1：是，0：否）', 'delete_flag', '791636597375643659', 4, 1000, 1, 'admin', '2020-12-29 08:39:18', 'admin', '2020-12-29 08:39:18', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793518987853312000', '793518520830144512', 'name', '791636597375643648', '建筑名称', 'name', '791636597375643654', 256, 2, 0, 'admin', '2020-12-29 08:41:10', 'admin', '2020-12-29 08:41:10', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793520073024614400', '793518520830144512', 'alias', '791636597375643648', '建筑简称', 'alias', '791636597375643654', 256, 3, 0, 'admin', '2020-12-29 08:45:28', 'admin', '2020-12-29 08:45:28', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793520607114702848', '793518520830144512', 'planId', '791636597375643648', '所属建设规划', 'plan_id', '791636597375643654', 256, 4, 0, 'admin', '2020-12-29 08:47:36', 'admin', '2020-12-29 08:47:36', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793520790783275008', '793518520830144512', 'description', '791636597375643648', '建筑描述、简介', 'description', '791636597375643654', 256, 5, 0, 'admin', '2020-12-29 08:48:19', 'admin', '2020-12-29 08:48:19', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793521050712682496', '793518520830144512', 'isUsed', '791636597375643648', '是否投入使用', 'is_used', '791636597375643654', 8, 6, 0, 'admin', '2020-12-29 08:49:21', 'admin', '2020-12-29 08:49:21', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793521315364876288', '793518520830144512', 'completedTime', '791636597375643650', '建成时间', 'completed_time', '791636597375643663', NULL, 7, 0, 'admin', '2020-12-29 08:50:24', 'admin', '2020-12-29 08:50:24', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793521526799740928', '793518520830144512', 'buildingAge', '791636597375643649', '楼龄', 'building_age', '791636597375643659', NULL, 8, 0, 'admin', '2020-12-29 08:51:15', 'admin', '2020-12-29 08:51:15', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793521960893427712', '793518520830144512', 'estateStatus', '791636597375643648', '物业状态：已出售，已出租，待租售', 'estate_status', '791636597375643654', 255, 9, 0, 'admin', '2020-12-29 08:52:58', 'admin', '2020-12-29 09:34:07', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793522189738848256', '793518520830144512', 'estateType', '791636597375643648', '物业性质', 'estate_type', '791636597375643654', 255, 10, 0, 'admin', '2020-12-29 08:53:53', 'admin', '2020-12-29 08:53:53', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793522519394365440', '793518520830144512', 'floorCount', '791636597375643649', '建筑层数', 'floor_count', '791636597375643659', NULL, 11, 0, 'admin', '2020-12-29 08:55:12', 'admin', '2020-12-29 08:55:12', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793524856259227648', '793518221281341440', 'name', '791636597375643648', '规划名称，如一期，二期', 'name', '791636597375643654', 128, 2, 0, 'admin', '2020-12-29 09:04:29', 'admin', '2020-12-29 09:04:29', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793525111658786816', '793518221281341440', 'startDate', '791636597375643650', '开始日期', 'start_date', '791636597375643663', NULL, 3, 0, 'admin', '2020-12-29 09:05:30', 'admin', '2020-12-29 09:05:30', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793525215157432320', '793518221281341440', 'endDate', '791636597375643650', '介绍日期', 'end_date', '791636597375643663', NULL, 4, 0, 'admin', '2020-12-29 09:05:54', 'admin', '2020-12-29 09:05:54', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793525852087660544', '793518221281341440', 'buildingArea', '791636597375643653', '建筑面积', 'building_area', '791636597375643661', 10, 5, 0, 'admin', '2020-12-29 09:08:26', 'admin', '2020-12-29 09:08:26', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793526110087688192', '793518221281341440', 'polygon', '791636597375643648', '规划区域，面', 'polygon', '791636597375643654', 256, 6, 0, 'admin', '2020-12-29 09:09:28', 'admin', '2020-12-29 09:09:28', 0);
INSERT INTO `t_demo_model_detail` VALUES ('793526302987923456', '793518221281341440', 'solid', '791636597375643648', '规划区域，空间', 'solid', '791636597375643654', 256, 7, 0, 'admin', '2020-12-29 09:10:14', 'admin', '2020-12-29 09:10:14', 0);

-- ----------------------------
-- Table structure for t_demo_property_column_pi
-- ----------------------------
DROP TABLE IF EXISTS `t_demo_property_column_pi`;
CREATE TABLE `t_demo_property_column_pi`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `property_type_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性类型ID',
  `column_type_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段类型ID',
  `sort_no` int(11) NULL DEFAULT 999 COMMENT '排序',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `modify_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `modify_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `delete_flag` smallint(4) NULL DEFAULT NULL COMMENT '是否删除（1：是，0：否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '属性、字段类型的关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_demo_property_column_pi
-- ----------------------------

-- ----------------------------
-- Table structure for t_demo_property_type
-- ----------------------------
DROP TABLE IF EXISTS `t_demo_property_type`;
CREATE TABLE `t_demo_property_type`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `qualified_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '全限定名（带包路径）',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `modify_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `modify_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `delete_flag` smallint(4) NULL DEFAULT NULL COMMENT '是否删除（1：是，0：否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '属性类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_demo_property_type
-- ----------------------------
INSERT INTO `t_demo_property_type` VALUES ('791636597375643648', 'String', 'java.lang.String', 3, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` VALUES ('791636597375643649', 'Integer', 'java.lang.Integer', 6, 'yyy', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` VALUES ('791636597375643650', 'Date', 'java.util.Date', 9, 'yyy', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` VALUES ('791636597375643651', 'Boolean', 'java.lang.Boolean', 12, 'yyy', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` VALUES ('791636597375643652', 'Float', 'java.lang.Float', 15, 'yyyy', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` VALUES ('791636597375643653', 'Double', 'java.lang.Double', 18, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);

SET FOREIGN_KEY_CHECKS = 1;
