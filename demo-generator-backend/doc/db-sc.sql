CREATE DATABASE IF NOT EXISTS `demo_generator_db` DEFAULT CHARACTER SET = utf8;
Use `demo_generator_db`;

-- ----------------------------
-- Table structure for t_demo_model
-- ----------------------------
drop table if exists `t_demo_model`;
create table `t_demo_model`(
    `id` varchar(32) comment '主键' ,
    `name` varchar(64) comment '模型名称（带限定名）',
    `remark` varchar(512) comment '模型说明',
    `table_name` varchar(64) comment '模型对应的表名称',
    `sort_no` bigint comment '排序',
    `model_name` varchar(64) comment '模型名称（不带限定名）',
    `package_dir` varchar(64) comment '父级包路径',
    `package_dir2` varchar(64) comment '二级包路径',
    `create_user` varchar(64) comment '创建人' ,
    `create_time` datetime comment '创建时间' ,
    `modify_user` varchar(64) comment '最后更新人' ,
    `modify_time` datetime comment '最后更新时间' ,
    `delete_flag` smallint(4) comment '是否删除(1是,0否)' ,
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模型（类）表';


-- ----------------------------
-- Table structure for t_demo_model_detail
-- ----------------------------
drop table if exists `t_demo_model_detail`;
create table `t_demo_model_detail`(
    `id` varchar(32) comment '主键' ,
    `model_id` varchar(32) comment '所属模型' ,
    `property_name` varchar(64) comment '属性名（符合驼峰命名）' ,
    `property_type_id` varchar(32) comment '属性类型ID' ,
    `remark` varchar(512) comment '属性说明' ,
    `column_name` varchar(64) comment '表字段名' ,
    `column_type_id` varchar(32) comment '表字段类型ID' ,
    `column_length` int comment '表字段长度' ,
    `sort_no` int comment '序号' ,
    `inherent_flag` tinyint default 0 comment '是否时固有的标记（1：不能删除，0：可删除（默认））' ,
    `create_user` varchar(64) comment '创建人' ,
    `create_time` datetime comment '创建时间' ,
    `modify_user` varchar(64) comment '最后更新人' ,
    `modify_time` datetime comment '最后更新时间' ,
    `delete_flag` smallint comment '是否删除（1是,0否）' ,
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模型明细表';


-- ----------------------------
-- Table structure for t_demo_property_type
-- ----------------------------
drop table if exists `t_demo_property_type`;
create table `t_demo_property_type`(
    `id` varchar(32) comment '主键',
    `name` varchar(64) comment '名称',
    `qualified_name` varchar(128) comment '全限定名（带包路径）',
    `sort_no` int comment '排序',
    `remark` varchar(512) comment '备注',
    `create_user` varchar(64) comment '创建人',
    `create_time` datetime comment '创建时间',
    `modify_user` varchar(64) comment '最后更新人',
    `modify_time` datetime comment '最后更新时间',
    `delete_flag` smallint(4) comment '是否删除（1：是，0：否）',
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='属性类型表';
-- ----------------------------
-- Table data for t_demo_property_type
-- ----------------------------
INSERT INTO `t_demo_property_type` (`id`, `name`, `qualified_name`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643648', 'String', 'java.lang.String', 3, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` (`id`, `name`, `qualified_name`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643649', 'Integer', 'java.lang.Integer', 6, 'yyy', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` (`id`, `name`, `qualified_name`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643650', 'Date', 'java.util.Date', 9, 'yyy', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` (`id`, `name`, `qualified_name`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643651', 'Boolean', 'java.lang.Boolean', 12, 'yyy', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` (`id`, `name`, `qualified_name`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643652', 'Float', 'java.lang.Float', 15, 'yyyy', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_property_type` (`id`, `name`, `qualified_name`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643653', 'Double', 'java.lang.Double', 18, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);


-- ----------------------------
-- Table structure for t_demo_column_type
-- ----------------------------
drop table if exists `t_demo_column_type`;
create table `t_demo_column_type`(
    `id` varchar(32) comment '主键',
    `name` varchar(64) comment '类型名称',
    `len` int comment '字段类型长度',
    `default_len` int comment '字段类型默认长度',
    `sort_no` int comment '排序',
    `remark` varchar(512) comment '备注',
    `create_user` varchar(64) comment '创建人',
    `create_time` datetime comment '创建时间',
    `modify_user` varchar(64) comment '最后更新人',
    `modify_time` datetime comment '最后更新时间',
    `delete_flag` smallint(4) comment '是否删除（1：是，0：否）',
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字段类型表';
-- ----------------------------
-- Table data for t_demo_column_type
-- ----------------------------
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643654', 'varchar', NULL, 32, 2, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643655', 'char', NULL, 240, 4, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643656', 'text', NULL, NULL, 6, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643657', 'int', NULL, NULL, 8, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643658', 'tinyint', NULL, NULL, 10, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643659', 'smallint', NULL, NULL, 12, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643660', 'bigint', NULL, NULL, 14, 'xxx', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643661', 'float', NULL, NULL, 16, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643662', 'double', NULL, NULL, 18, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643663', 'date', NULL, NULL, 20, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643664', 'datetime', NULL, NULL, 22, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643665', 'timestamp', NULL, NULL, 24, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0);
INSERT INTO `t_demo_column_type` (`id`, `name`, `len`, `default_len`, `sort_no`, `remark`, `create_user`, `create_time`, `modify_user`, `modify_time`, `delete_flag`) VALUES ('791636597375643666', 'json', NULL, NULL, 26, '', 'admin', '2020-12-24 04:01:13', 'admin', '2020-12-24 04:01:13', 0


-- ----------------------------
-- Table structure for t_demo_property_column_pi
-- ----------------------------
drop table if exists `t_demo_property_column_pi`;
create table `t_demo_property_column_pi`(
    `id` varchar(32) comment '主键',
    `property_type_id` varchar(32) comment '属性类型ID',
    `column_type_id` varchar(32) comment '字段类型ID',
    `sort_no` int default 999 comment '排序',
    `remark` varchar(512) comment '备注',
    `create_user` varchar(64) comment '创建人',
    `create_time` datetime comment '创建时间',
    `modify_user` varchar(64) comment '最后更新人',
    `modify_time` datetime comment '最后更新时间',
    `delete_flag` smallint(4) comment '是否删除（1：是，0：否）',
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='属性、字段类型的关系表';