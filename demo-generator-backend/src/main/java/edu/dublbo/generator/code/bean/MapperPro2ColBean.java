package edu.dublbo.generator.code.bean;

/**
 * @author habitplus
 * @since 2020-12-26 11:20
 */
public class MapperPro2ColBean {
    private String proName;
    private String colName;

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }
}
