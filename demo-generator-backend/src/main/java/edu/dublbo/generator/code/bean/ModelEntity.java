package edu.dublbo.generator.code.bean;

import java.util.List;
import java.util.Set;

public class ModelEntity {
    // 模型名称（不带包路径）
    private String modelName;
    // 模型的包路径（精确到模块）
    private String modelPackageDir;
    // 模型描述
    private String modelDesc;
    // 模型对应的表名称
    private String tableName;
    // Services 类名称
    private String serviceName;
    // Controller 类名称
    private String controllerName;
    // Mapper 类名称
    private String mapperName;
    // 模块名称
    private String moduleName;
    // 请求路径
    private String requestPath;

    // 模型明细数据
    private List<ModelDetailEntity> details;
    // 需要额外导入的包集合
    private Set<String> importPackageSet;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelPackageDir() {
        return modelPackageDir;
    }

    public void setModelPackageDir(String modelPackageDir) {
        this.modelPackageDir = modelPackageDir;
    }

    public String getModelDesc() {
        return modelDesc;
    }

    public void setModelDesc(String modelDesc) {
        this.modelDesc = modelDesc;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public String getMapperName() {
        return mapperName;
    }

    public void setMapperName(String mapperName) {
        this.mapperName = mapperName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public List<ModelDetailEntity> getDetails() {
        return details;
    }

    public void setDetails(List<ModelDetailEntity> details) {
        this.details = details;
    }

    public Set<String> getImportPackageSet() {
        return importPackageSet;
    }

    public void setImportPackageSet(Set<String> importPackageSet) {
        this.importPackageSet = importPackageSet;
    }

}
