package edu.dublbo.generator.code.bean;

public class ModelDetailEntity {
    // 属性名
    private String proName;
    // 属性类型（不含限定名），如 Date，而不是 java.util.Date
    private String proType;
    // 表字段名
    private String colName;
    // 表字段类型（带长度）
    private String colType;
    // 字段注释
    private String colDesc;

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getColDesc() {
        return colDesc;
    }

    public void setColDesc(String colDesc) {
        this.colDesc = colDesc;
    }

    @Override
    public String toString() {
        return "ModelDetailEntity{" +
                "proName=" + proName + ", " +
                "proType='" + proType + ", " +
                "colName='" + colName + ", " +
                "colType='" + colType + ", " +
                "colDesc='" + colDesc + ", " +
                "}";
    }
}
