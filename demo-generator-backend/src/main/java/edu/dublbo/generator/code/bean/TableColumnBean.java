package edu.dublbo.generator.code.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author habitplus
 * @since 2020-12-25 14:52
 */
public class TableColumnBean {
    private String colName;
    private String colType;
    private String colDesc;

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getColDesc() {
        return colDesc;
    }

    public void setColDesc(String colDesc) {
        this.colDesc = colDesc;
    }
}
