package edu.dublbo.generator.code.bean;

/**
 * @author habitplus
 * @since 2020-12-25 16:33
 */
public class ModelPropertyBean {
    private String proDesc;
    private String proType;
    private String proName;

    public String getProDesc() {
        return proDesc;
    }

    public void setProDesc(String proDesc) {
        this.proDesc = proDesc;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }
}
