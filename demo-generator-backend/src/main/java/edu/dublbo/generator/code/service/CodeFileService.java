package edu.dublbo.generator.code.service;

import edu.dublbo.generator.basic.entity.TDemoModel;
import edu.dublbo.generator.basic.entity.TDemoModelDetail;
import edu.dublbo.generator.basic.mapper.TDemoModelDetailMapper;
import edu.dublbo.generator.basic.mapper.TDemoModelMapper;

import edu.dublbo.generator.code.dao.CodeFileGenDAO;
import edu.dublbo.generator.common.exception.OptErrorException;
import edu.dublbo.generator.common.result.OptStatus;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.zip.ZipOutputStream;

@Service
public class CodeFileService {
    private static final Logger logger = LoggerFactory.getLogger(CodeFileService.class);

    @Autowired
    TDemoModelMapper modelMapper;
    @Autowired
    TDemoModelDetailMapper modelDetailMapper;


    @Transactional
    public Map<String, Object> loadCode(String modelId) {
        TDemoModel model = modelMapper.get(modelId);

        Map<String, Object> con = new HashMap<>();
        con.put("modelId", model.getId());
        con.put("_order", "sort_no");
//        con.put("inherentFlag", "0"); // 排除固有字段
        List<TDemoModelDetail> detailList = modelDetailMapper.select(con);

        return CodeFileGenDAO.generateCode(model, detailList);
    }

//    public void deleteTmpDemoFile() {
//        String proDir = System.getProperty("user.dir");
//        if (StringUtils.isEmpty(proDir)) {
//            throw new OptErrorException(OptStatus.FAIL.getOptCode(), "获取的项目根路径为空");
//        }
//        String rootDir = proDir + "/tmpdemo";
//        FileOperator.deleteFile(new File(rootDir));
//    }

    // 通过 多个模型id 生成多个模型代码
    public void generateCode(Set<String> idSet, OutputStream outputStream) {
        if (idSet == null || idSet.size() == 0)
            throw new OptErrorException(OptStatus.FAIL.getOptCode(), "请求参数为空");
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(outputStream);
        for (String modelId : idSet) {
            if (StringUtils.isNotBlank(modelId)) {
                // 查询模型信息
                TDemoModel model = modelMapper.get(modelId);
                // 查询模型明细
                Map<String, Object> con = new HashMap<>();
                con.put("modelId", modelId);
                con.put("_order", "sort_no");
                List<TDemoModelDetail> modelDetails = modelDetailMapper.select(con);

                CodeFileGenDAO.zipCode(model, modelDetails, zos);
            }
        }
//        byte[] bytes = outputStream.toByteArray();
        // 关闭压缩流
        IOUtils.closeQuietly(zos);
//        return bytes;
    }
}
