package edu.dublbo.generator.code.dao;

import edu.dublbo.generator.code.cto.DemoPackageCTO;
import edu.dublbo.generator.common.exception.OptErrorException;
import edu.dublbo.generator.common.result.OptStatus;
import edu.dublbo.generator.utils.Constant;
import edu.dublbo.generator.utils.FileOperator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @author habitplus
 * @since 2020-12-26 23:50
 */
public class CodeFileZipDAO {
//    private static final Logger logger = LoggerFactory.getLogger(CodeFileZipDAO.class);
//    // 模型名称
//    private String modelName;
//    // 模型的包路径（精确到模块）
//    private String modelPackageDir;
//
//    public CodeFileZipDAO(String modelName, String modelPackageDir) {
//        this.modelName = modelName;
//        this.modelPackageDir = modelPackageDir;
//    }
//
//    public synchronized File[] generateModelZipFile(DemoPackageCTO content) {
//        String zipFileName = modelName + "_" + System.currentTimeMillis();
//        String packagePath = packageToFilePath(modelPackageDir);
//        String zipRootDir = Constant.CACHE_DIR + zipFileName;
//        String projectPath = zipRootDir + "/" + packagePath;
//        String serviceName = CodeFileGenDAO.getBusiBeanName(modelName);
//        logger.info("zip root dir: {}\nproject path: {}", zipRootDir, projectPath);
//
//        FileOperator.writeContent(content.getTableDemo(), projectPath + "/model", modelName + "Mysql.sql");
//        FileOperator.writeContent(content.getEntityDemo(), projectPath + "/model", modelName + ".java");
//        FileOperator.writeContent(content.getMapperInterDemo(), projectPath + "/mapper", modelName + "Mapper.java");
//        FileOperator.writeContent(content.getMapperXmlDemo(), projectPath + "/mapper", modelName + "Mapper.xml");
//        FileOperator.writeContent(content.getServiceDemo(), projectPath + "/service", serviceName + "Service.java");
//        FileOperator.writeContent(content.getControllerDemo(), projectPath + "/controller", serviceName + "Controller.java");
//
//        String zipFilepath = Constant.CACHE_DIR + zipFileName + ".zip";
//        File zipRootDirFile = new File(zipRootDir);
//        File zipFile = new File(zipFilepath);
//        FileOperator.toZip(zipRootDirFile, zipFile, true);
//
//        return new File[]{zipFile, zipRootDirFile};
//    }
//
//    private String packageToFilePath(String packageDir) {
//        if (StringUtils.isEmpty(packageDir)) throw new OptErrorException(OptStatus.FAIL.getOptCode(), "操作的项目模块目录不存在");
//        return packageDir.replace(".", "/");
//    }
}
