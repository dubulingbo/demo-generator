package edu.dublbo.generator.config;

import edu.dublbo.generator.common.interceptor.JwtInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author habitplus
 * @since 2020-12-27 22:14
 */
//@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    private static final Logger logger = LoggerFactory.getLogger(InterceptorConfig.class);

    public void addInterceptors(InterceptorRegistry registry) {
        // 添加拦截规则
        logger.info("拦截器起作用。。。");
        registry.addInterceptor(new JwtInterceptor()).excludePathPatterns("/auth/user/login");
    }
}
