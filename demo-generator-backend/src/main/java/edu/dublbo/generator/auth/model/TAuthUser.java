package edu.dublbo.generator.auth.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 权限用户 实体类
 * @author guan_exe (demo-generator)
 * @since 2020年12月27日 17:25:15.58
 * i believe i can i do
 */
public class TAuthUser implements Serializable {
//    private static final long serialVersionUID = serialNumberL;
    // 主键ID
    private String id;
    // 用户名
    private String username;
    // 昵称
    private String nickname;
    // 邮箱
    private String email;
    // 性别
    private Integer gender;
    // 密码
    private String password;
    // 角色ID
    private Integer roleId;
    // 角色名称
    private String roleName;
    // 头像
    private String avatar;
    // 账号状态（0：未激活，1：已激活，2：提示修改密码，3：已过期）
    private String accountStatus;
    // 创建人
    private String createUser;
    // 创建时间
    private Date createTime;
    // 最后更新人
    private String modifyUser;
    // 最后更新时间
    private Date modifyTime;
    // 是否删除（1：是，0：否）
    private Integer deleteFlag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @Override
    public String toString() {
        return "TAuthUser{id="+id+", username="+username+", nickname="+nickname+", email="+email+", gender="+gender+", password="+password+", roleId="+roleId+", roleName="+roleName+", avatar="+avatar+", accountStatus="+accountStatus+", createUser="+createUser+", createTime="+createTime+", modifyUser="+modifyUser+", modifyTime="+modifyTime+", deleteFlag="+deleteFlag+"}";
    }
}
