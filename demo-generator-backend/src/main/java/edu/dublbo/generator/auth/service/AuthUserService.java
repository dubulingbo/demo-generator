package edu.dublbo.generator.auth.service;

import edu.dublbo.generator.auth.mapper.TAuthUserMapper;
import edu.dublbo.generator.auth.model.TAuthUser;
import edu.dublbo.generator.common.exception.OptErrorException;
import edu.dublbo.generator.common.result.OptStatus;
import edu.dublbo.generator.common.utils.JwtUtils;
import edu.dublbo.generator.common.utils.RedisUtil;
import edu.dublbo.generator.common.utils.SnowflakeIdWorker;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 权限用户Service类
 *
 * @author Administrator
 * @since 2020/9/12 17:14,星期六
 * Always believe that something wonderful is about to happen.
 **/
@Service
public class AuthUserService {
    public static final Logger logger = LoggerFactory.getLogger(AuthUserService.class);

    @Autowired
    private SnowflakeIdWorker idWorker;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private TAuthUserMapper mapper;

    @Autowired
    private JwtUtils jwtUtils;

    private final String username = "admin";
    private final String password = "12345678";


    public Map<String, String> get(String username, String password) {
        Map<String, String> userMap = new HashMap<>();
        if (!StringUtils.isBlank(username) && this.username.equals(username) && this.password.equals(password)) {
            // 生成 token
            String token = RandomStringUtils.random(8, true, true)
                    + idWorker.nextStringId() + RandomStringUtils.random(6, true, false);
            userMap.put("username", this.username);
            userMap.put("password", this.password);
            userMap.put("token", token);
            redisUtil.set(username, token, 3 * 60 * 60);
        }
        return userMap;
    }


    @Transactional
    public void save(TAuthUser entity){
        mapper.add(entity);
    }

    @Transactional
    public void delete(String id){
        mapper.delete(id);
    }

    @Transactional
    public void update(TAuthUser entity){
        mapper.update(entity);
    }

    @Transactional
    public TAuthUser get(String id){
        return mapper.get(id);
    }

    @Transactional
    public List<TAuthUser> listAll(){
        return mapper.listAll();
    }

    @Transactional
    public List<TAuthUser> select(Map<String,Object> condition){
        return mapper.select(condition);
    }

    public String valid(String username, String password) {
        Map<String, Object> condition = new HashMap<>();
        String dePwd = DigestUtils.md5DigestAsHex(password.getBytes());
        condition.put("username", username);
        condition.put("password", dePwd);
        condition.put("accountStatus", "1");

        List<TAuthUser> userList = mapper.select(condition);

        if (userList == null || userList.size() != 1) {
            throw new OptErrorException(OptStatus.FAIL.getOptCode(), "用户名或密码错误");
        }

        // 用户名密码正确，生成 token 字段
        String id =  userList.get(0).getId();
        if (StringUtils.isEmpty(id)) throw new OptErrorException(OptStatus.FAIL.getOptCode(), "用户ID不存在");

        logger.info("用户的ID：{}", id);
        return jwtUtils.sign(username, id);
    }
}
