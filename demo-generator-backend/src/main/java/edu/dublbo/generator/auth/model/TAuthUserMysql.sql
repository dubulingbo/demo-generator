-- @author guan_exe (demo-generator)
-- @date 2020年12月27日 17:25:14.983
-- @desc 权限用户表
drop table if exists t_auth_user;
create table t_auth_user(
	id varchar(32) comment '主键ID',
	username varchar(32) comment '用户名',
	nickname varchar(64) comment '昵称',
	email varchar(64) comment '邮箱',
	gender tinyint comment '性别',
	password varchar(64) comment '密码',
	role_id int comment '角色ID',
	role_name varchar(32) comment '角色名称',
	avatar varchar(128) comment '头像',
	account_status varchar(32) comment '账号状态（0：未激活，1：已激活，2：提示修改密码，3：已过期）',
	create_user varchar(64) comment '创建人',
	create_time datetime comment '创建时间',
	modify_user varchar(64) comment '最后更新人',
	modify_time datetime comment '最后更新时间',
	delete_flag smallint(4) comment '是否删除（1：是，0：否）',
	primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限用户 表';
