package edu.dublbo.generator.auth.mapper;

import java.util.Map;
import java.util.List;
import edu.dublbo.generator.auth.model.TAuthUser;

/**
 * 权限用户 Mapper接口
 * @author guan_exe (demo-generator)
 * @since 2020年12月27日 17:25:15.128
 * i believe i can i do
 */
public interface TAuthUserMapper {
    // 添加
    int add(TAuthUser entity);

    // 删除
    int delete(String id);

    // 更新
    int update(TAuthUser entity);

    // 根据主键获取
    TAuthUser get(String id);

    // 获取全部数据
    List<TAuthUser> listAll();

    // 根据条件查询
    List<TAuthUser> select(Map<String, Object> condition);

    // 获取记录数
    Integer count();
}
