<#noparse><?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Config 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
</#noparse>
<mapper namespace="${model.modelPackageDir}.mapper.${model.mapperName}">
    <!-- 定义表名 -->
    <sql id="tableName">${model.tableName}</sql>

    <!-- 定义全部列名 -->
    <sql id="columnName">
        <#list model.details as it>${it.colName}<#sep>,</#sep></#list>
    </sql>

    <!-- 将表中的各字段映射到实体类中的相应属性 -->
    <resultMap id="${model.modelName?uncap_first}" type="${model.modelPackageDir}.model.${model.modelName}">
    <#list model.details as item>
        <result column="${item.colName}" property="${item.proName}"/>
    </#list>
    </resultMap>

    <!-- 不带表别名的判断条件语句 -->
    <sql id="condition">
    <#list model.details as item><#if item.proName != "id">
        <if test="${item.proName} != null and ${item.proName} != ''">
            and ${item.colName} = ${"#\{" + item.proName + "}"}
        </if>
    </#if></#list>
    </sql>

    <!-- 带表别名 a 的判断条件语句 -->
    <sql id="aliasCondition">
    <#list model.details as item><#if item.proName != "id">
        <if test="${item.proName} != null and ${item.proName} != ''">
            and a.${item.colName} = ${"#\{" + item.proName + "}"}
        </if>
    </#if></#list>
    </sql>

    <!-- 全字段插入语句 -->
    <insert id="add" parameterType="${model.modelPackageDir}.model.${model.modelName}">
        <#noparse>insert into <include refid="tableName"/> (
            <include refid="columnName"/>
        ) values (</#noparse>
        <#list model.details as item>
            ${"#\{" + item.proName + "}"}<#sep>,</#sep>
        </#list>
        )
    </insert>

    <!-- 删除语句 -->
    <#noparse>
    <delete id="delete" parameterType="String">
        delete from <include refid="tableName"/> where id = #{id}
    </delete>
    </#noparse>

    <!-- 全字段更新语句（除id） -->
    <update id="update" parameterType="${model.modelPackageDir}.model.${model.modelName}">
        update <#noparse><include refid="tableName"/></#noparse>
        set
        <trim suffixOverrides=",">
        <#list model.details as item><#if item.proName != "id">
            ${item.colName} = ${"#\{" + item.proName + "}"}<#sep>,</#sep>
        </#if></#list>
        </trim>
        where id = ${"#\{id}"}
    </update>

    <!-- 通过 Id 查询某条记录 -->
    <select id="get" parameterType="String" resultMap="${model.modelName?uncap_first}">
        <#noparse>
        select <include refid="columnName"/>
        from <include refid="tableName"/>
        where id = #{id} and delete_flag = 0
        </#noparse>
    </select>

    <!-- 查询全部记录 -->
    <select id="listAll" resultMap="${model.modelName?uncap_first}">
        <#noparse>
        select <include refid="columnName"/>
        from <include refid="tableName"/>
        where delete_flag = 0
        </#noparse>
    </select>

    <!-- 按条件查询记录 -->
    <select id="select" parameterType="Map" resultMap="${model.modelName?uncap_first}">
        <#noparse>
        select <include refid="columnName"/>
        from <include refid="tableName"/>
        where delete_flag = 0
            <include refid="aliasCondition"/>
        <if test="_order != null and _order != ''">
            order by ${_order}
            <if test="_sort != null and _sort != ''">
                ${_sort}
            </if>
        </if>
        </#noparse>
    </select>

    <!-- 查询全部记录数（带条件） -->
    <#noparse>
    <select id="count" resultType="java.lang.Integer" parameterType="Map">
        select count(1)
        from <include refid="tableName"/>
        where delete_flag = 0
            <include refid="condition"/>
    </select>
    </#noparse>
</mapper>