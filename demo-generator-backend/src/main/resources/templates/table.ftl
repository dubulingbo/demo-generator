-- @author ${author}
-- @date ${createTime}
-- @desc ${model.modelDesc}表
drop table if exists `${model.tableName}`;
create table `${model.tableName}` (
<#list model.details as column>
	`${column.colName}` ${column.colType} comment '${column.colDesc}',
</#list>
	primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='${model.modelDesc} 表';