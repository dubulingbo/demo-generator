package ${model.modelPackageDir}.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ${model.modelPackageDir}.model.${model.modelName};
import ${model.modelPackageDir}.mapper.${model.mapperName};

/**
 * ${model.modelDesc} Service类
 * @author ${author}
 * @since ${createTime}
 * Always believe that something wonderful is about to happen
 */
@Service
public class ${model.serviceName} {

    @Autowired
    private ${model.mapperName} mapper;

    @Transactional
    public void save(${model.modelName} entity){
        mapper.add(entity);
    }

    @Transactional
    public void delete(String id){
        mapper.delete(id);
    }

    @Transactional
    public void update(${model.modelName} entity){
        mapper.update(entity);
    }

    @Transactional
    public ${model.modelName} get(String id){
        return mapper.get(id);
    }

    @Transactional
    public List<${model.modelName}> listAll(){
        return mapper.listAll();
    }

    @Transactional
    public List<${model.modelName}> select(Map<String,Object> condition){
        return mapper.select(condition);
    }
}
