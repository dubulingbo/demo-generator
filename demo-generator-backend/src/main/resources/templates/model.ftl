package ${model.modelPackageDir}.model;

import java.io.Serializable;
<#list model.importPackageSet as p>
import ${p};
</#list>

/**
 * ${model.modelDesc} 实体类
 * @author ${author}
 * @since ${createTime}
 * i believe i can i do
 */
public class ${model.modelName} implements Serializable {
<#list model.details as pro>
    // ${pro.colDesc}
    private ${pro.proType} ${pro.proName};
</#list>

<#list model.details as pro>
    public ${pro.proType} get${pro.proName?cap_first}() {
        return ${pro.proName};
    }

    public void set${pro.proName?cap_first}(${pro.proType} ${pro.proName}) {
        this.${pro.proName} = ${pro.proName};
    }<#sep>${"\n"}</#sep>
</#list>

    @Override
    public String toString() {
        return "${model.modelName}{" +
                <#list model.details as pro>"${pro.proName}=" + ${pro.proName} + ", " +<#sep>${"\n"}</#sep></#list>
                "}";
    }
}
