package ${model.modelPackageDir}.mapper;

import java.util.Map;
import java.util.List;
import ${model.modelPackageDir}.model.${model.modelName};

/**
 * ${model.modelDesc} Mapper接口
 * @author ${author}
 * @since ${createTime}
 * i believe i can i do
 */
public interface ${model.mapperName} {
    // 添加
    int add(${model.modelName} entity);

    // 删除
    int delete(String id);

    // 更新
    int update(${model.modelName} entity);

    // 根据主键获取
    ${model.modelName} get(String id);

    // 获取全部数据
    List<${model.modelName}> listAll();

    // 根据条件查询
    List<${model.modelName}> select(Map<String, Object> condition);

    // 获取记录数
    Integer count();
}