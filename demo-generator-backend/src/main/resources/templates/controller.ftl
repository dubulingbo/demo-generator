package ${model.modelPackageDir}.controller;

import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ${model.modelPackageDir}.model.${model.modelName};
import ${model.modelPackageDir}.service.${model.serviceName};

/**
 * ${model.modelDesc} 控制类
 * @author ${author}
 * @since ${createTime}
 **/
@RestController
// @RequestMapping(value = "${model.requestPath}")
// @Api(tags = "${model.modelDesc}")
public class ${model.controllerName} {
    @Autowired
    private ${model.serviceName} service;

    // 增加${model.modelDesc}
//    @ApiOperation(value = "添加${model.modelDesc}")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "page", value = "分页起始位置", required = true, dataType = "Integer"),
//            @ApiImplicitParam(name = "limit", value = "分页结束位置", required = true, dataType = "Integer"),
//    })
    @PostMapping
    public ${model.modelName} add(@Validated ${model.modelName} entity, HttpServletRequest request){
        <#noparse>
        // 主键生成策略，可自定义
        String id = UUID.randomUUID().toString().replace("-","");
        // logger.info("Generate id is : {}", id);
        entity.setId(id);
        entity.setCreateTime(new Date());
        entity.setModifyTime(entity.getCreateTime());
        // 这里的用户可以自定义，默认为 admin
        entity.setCreateUser("admin");
        entity.setModifyUser("admin");
        entity.setDeleteFlag(0);
        service.save(entity);
        return entity;</#noparse>
    }

    // 修改${model.modelDesc}
    @PutMapping(value = "/{id}")
    public ${model.modelName} edit(@PathVariable(value = "id") String id, @RequestBody ${model.modelName} modifyEntity, HttpServletRequest request) {
        ${model.modelName} oldEntity = service.get(id);
        <#noparse>
        if (oldEntity != null) {
            // 这里需根据业务需求修改哪些字段，
            // 但是 id,createUser,createTime,modifyUser,modifyTime,deleteFlag 这些字段不允许用户修改
            // oldEntity 为修改前的对象，modifyEntity 为修改后的对象，该怎么做自己根据业务需求自己定制吧，这里就不生成了
            // Do what you want to do
            modifyEntity.setId(id);
            modifyEntity.setModifyTime(new Date());
            modifyEntity.setModifyUser("admin");
            service.update(modifyEntity);
        }
        return modifyEntity;</#noparse>
    }

    // 获取${model.modelDesc}的基本信息
    @GetMapping(value = "/{id}")
    public ${model.modelName} detail(@PathVariable(value = "id") String id) {
        ${model.modelName} entity = service.get(id);
        return entity;
    }

    // 删除${model.modelDesc}
    @DeleteMapping(value = "/{id}")
    public ${model.modelName} delete(@PathVariable(value = "id") String id) {
        ${model.modelName} entity = service.get(id);
        entity.setModifyUser("admin");
        entity.setModifyTime(new Date());
        entity.setDeleteFlag(1);
        service.update(entity);
        return entity;
    }
}